#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <string>
using namespace std;

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
namespace PO = boost::program_options;

#include "src/bayeslex/BayesLex.hpp"
namespace DATA  = BayesLex::Data;
namespace BASE  = BayesLex::Baseline;
namespace STATS = BayesLex::Stats;
namespace EVAL  = BayesLex::Eval;
namespace OPT   = BayesLex::Optimize;
namespace ADMM  = BayesLex::ADMM;

int main(int argc, char const *argv[]) {
  PO::options_description desc("Option List");
  desc.add_options()
      ("help",      "produce help message")
      ("epochs",      PO::value<int>()->default_value(250),   "num epochs")
      ("epoch-iters", PO::value<int>()->default_value(5),     "iters per epoch")
      ("max_vocab",   PO::value<int>()->default_value(50000), "maximum vocab size")
      ("prefix",      PO::value<string>()->required(),        "bow prefix")
      ("poslex",      PO::value<string>()->required(),        "positive lexicon")
      ("neglex",      PO::value<string>()->required(),        "negative lexicon")
      ("max_k",       PO::value<double>()->default_value(0.9), "optimization param")
      ("admm_rho",    PO::value<double>()->default_value(1.0), "optimization param")
  ;

  // Loads command line args and will notify if missing requirement
  PO::variables_map vm;
  PO::store(PO::parse_command_line(argc, argv, desc), vm);
  PO::notify(vm);

  // Display description of command line args
  if (vm.count("help")) {
      cout << desc << endl;
      return 1;
  }

  // Generates:
  // <0> = data.labels - Vector where index corresponds to matrix row, 1 = POS : -1 = NEG
  // <1> = data.x      - CSR Matrix, each row is a document, each row is a word in vocab
  // <2> = data.vocab  - map of words to matrix column
  auto data = DATA::load_data(vm["prefix"].as<const string>(),
                              vm["max_vocab"].as<const int>());

  Row_Vector&           labels  = get<0>(data);
  CSR_Matrix&           x       = get<1>(data);
  map<string, double>&  vocab   = get<2>(data);

  // Loaded data metrics
  auto prev = cout.fill('-');
  cout << endl << "Loading Data" << endl
       << setw(35) << "" << endl;
  cout.fill(prev);
  cout << left;
  cout << setw(25) << "Document Count: "        << setw(10) << labels.size() << endl
       << setw(25) << "SparseMatrix Entries: "  << setw(10) << x.nonZeros() << endl
       << setw(25) << "Vocab Index Size: "      << setw(10) << vocab.size() << endl;
  cout << endl;

  // Vector such that:
  //    - vocab[word] is a unique ID for each word
  //    - if word is in lexicon, its ID will appear in the vector
  Row_Vector poslex = DATA::get_lexicon(vm["poslex"].as<const string>(), vocab);
  Row_Vector neglex = DATA::get_lexicon(vm["neglex"].as<const string>(), vocab);

  // Vocab/Lexicon Metrics
  cout.fill('-');
  cout << endl << "Defining Lexicons" << endl
       << setw(35) << "" << endl;
  cout.fill(prev);
  cout << left;
  cout << setw(25) << "Positive Lexicon Size: "  << setw(10) << poslex.size() << endl
       << setw(25) << "Negative Lexicon Size: "  << setw(10) << neglex.size() << endl;
  cout << endl;

  // Baseline classifier is a simple lexical count classifier
  Row_Vector clf = BASE::get_lex_classifier(poslex, neglex, vocab);
  Row_Vector baseline_predictions = clf*x.transpose();
  cout << EVAL::get_results(labels,STATS::scale(baseline_predictions, x), "baseline");

  OPT::BayesLexOptimizer opt(x, poslex, neglex, vm["max_k"].as<const double>());

  cout.fill('-');
  cout << endl << "Starting ADMM Optimization" << endl
       << setw(35) << "" << endl;
  cout.fill(prev);

  opt.estimateADDM(vm["epochs"].as<const int>(),
                   vm["epoch-iters"].as<const int>(),
                   vm["admm_rho"].as<const double>(),
                   vm["max_k"].as<const double>());
  Row_Vector mom_predictions = STATS::makePredictionsKPerWord(x, opt.poslex, opt.neglex,
                                                             opt.pos_k, opt.neg_k);
  cout << EVAL::get_results(labels,STATS::scale(mom_predictions, x), "lexicon MoM");

  return 0;
}
