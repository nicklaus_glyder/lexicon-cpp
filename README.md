# Lexicon Based MoM Classification #

Nick Glyder, Luke Morrow

Attempt at CPP implementation of the ADMM Bayesian Classifier

# How to run #

You must have Boost installed to compile and run the program

`sudo apt-get install libboost-all-dev`

The Eigen Matrix library is included in the `lib` directory.

Simply execute `make run` to compile and run the program on the Cornell DataSet. 