#include "BayesLex.hpp"

Row_Vector
BayesLex::Baseline::get_lex_classifier(
  const Row_Vector& poslex, const Row_Vector& neglex, const map<string, double>& vocab) {
  CSR_Matrix clf(1, vocab.size());
  vector<Triplet<double>> clf_triples;

  for (int i = 0; i < neglex.cols(); i++)
    clf_triples.emplace_back(Triplet<double>(0, neglex[i], -1));

  for (int i = 0; i < poslex.cols(); i++)
    clf_triples.emplace_back(Triplet<double>(0, poslex[i], 1));

  clf.setFromTriplets(clf_triples.begin(), clf_triples.end());
  return Row_Vector(clf);
}
