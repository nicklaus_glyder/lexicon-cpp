#include "BayesLex.hpp"

using namespace BayesLex::Optimize;
namespace STATS = BayesLex::Stats;
namespace ADMM = BayesLex::ADMM;

BayesLexOptimizer::BayesLexOptimizer(const CSR_Matrix& x,
                                     const Row_Vector& poslex,
                                     const Row_Vector& neglex,
                                     double max_k)
  : poslex(poslex), neglex(neglex), n_pos(poslex.size()), n_neg(neglex.size()),
      s(STATS::computeS(x)), max_k(max_k), eMu(STATS::eMu(x)),
      pos_mu(poslex.size()), neg_mu(neglex.size()) {

    // Co occurance counts over corpus
    auto co_count_tuple = STATS::getCoCountsTwoLex(x, poslex, neglex, s);
    co_count    = get<0>(co_count_tuple);
    e_co_count  = get<1>(co_count_tuple);

    for(int i=0; i < poslex.size(); ++i){
      pos_mu.col(i) = eMu.col(poslex[i]);
    }

    for(int i=0; i < neglex.size(); ++i){
      neg_mu.col(i) = eMu.col(neglex[i]);
    }

    pos_co_counts = co_count * Col_Vector::Ones(co_count.cols());
    neg_co_counts = co_count.transpose() * Col_Vector::Ones(co_count.rows());

    // Initial estimate for weights
    rval = STATS::estimateK(co_count_tuple);
    ratio = sqrt(neg_mu.sum() / pos_mu.sum());

    pos_k.setConstant(n_pos, rval*ratio);
    neg_k.setConstant(n_neg, rval / ratio);

    x_sum = x.sum();
}

//This is the biconvex method in Sec 9 of Boyd et al.
//This solves the optimization problem, and sets updated values for pos_k and neg_k
using namespace BayesLex::Optimize;
void
BayesLexOptimizer::estimateADDM(const int num_epochs,
                                const int max_iter,
                                double rho,
                                const double max_k) {
  double u = 0;
  for (int i = 0; i < num_epochs; i++) {
    // solve k_pos = argmin(k_pos \in C) F(k_pos,k_neg) + penalty
    auto LRQP_tuple =   this->getLowRankQuadraticParams(this->pos_co_counts, this->neg_co_counts,
                                                        this->pos_mu, this->neg_mu,
                                                        this->neg_k, this->s, u, rho);
    auto P_diag =       get<0>(LRQP_tuple);
    auto P_low_rank =   get<1>(LRQP_tuple);
    auto q =            get<2>(LRQP_tuple);

    this->pos_k = ADMM::admmQuadBounded(Row_Vector(P_diag),
                                        P_low_rank,
                                        Col_Vector(q),
                                        max_iter, 1, max_k);

    // Same as above, but for negative scores
    LRQP_tuple =   this->getLowRankQuadraticParams(this->neg_co_counts, this->pos_co_counts,
                                                    this->neg_mu, this->pos_mu,
                                                    this->pos_k, this->s, u*-1, rho);
    P_diag =       get<0>(LRQP_tuple);
    P_low_rank =   get<1>(LRQP_tuple);
    q =            get<2>(LRQP_tuple);

    Row_Vector old_neg_k = this->neg_k;
    this->neg_k = ADMM::admmQuadBounded(Row_Vector(P_diag),
                                        P_low_rank,
                                        Col_Vector(q),
                                        max_iter, 1, max_k);

    // update dual parameter u
    double violation = this->pos_mu.dot(this->pos_k) - this->neg_mu.dot(this->neg_k);
    u += violation;

    // compute residuals (Boyd et al, page 18
    double dual_residual = pow((rho * this->pos_mu * this->neg_mu.dot(this->neg_k - old_neg_k)).norm(), 2);
    double primal_residual = pow(violation,2);

    // update Rho
    auto update_rho_u = ADMM::updateRho(u,rho,primal_residual,dual_residual);

    rho = get<0>(update_rho_u);
    u = get<1>(update_rho_u);

    // Test residuals to see if have converged
    double eps_abs = .001 / this->x_sum;
    double eps_rel = .001 / this->x_sum;
    double eps_primal = 1.0 * eps_abs + eps_rel * max(this->pos_mu.dot(this->pos_k),
                                                      this->neg_mu.dot(this->neg_k));
    double eps_dual1 = sqrt(this->pos_k.size()) * eps_abs
                     + eps_rel * pow((u * this->pos_mu).norm(), 2);
    double eps_dual2 = sqrt(this->neg_k.size()) * eps_abs
                     + eps_rel * pow((u * this->neg_mu).norm(), 2);
    if (dual_residual < eps_dual1 + eps_dual2 && primal_residual < eps_primal) {
      cout << "Optimization Complete: " << endl
           << "iteration: " << i << endl
           << "dual: " << dual_residual << " < min(" << eps_dual1 << "," << eps_dual2 << ")" << endl
           << "primal: " << primal_residual << " < " << eps_primal << endl << endl;
      return;
    }
  }
}

tuple<Row_Vector, Dense_Matrix, Col_Vector, double>
  BayesLexOptimizer::getLowRankQuadraticParams(const Col_Vector& pos_co_counts,
                                               const Col_Vector& neg_co_counts,
                                               const Row_Vector& pos_mu,
                                               const Row_Vector& neg_mu,
                                               const Row_Vector& neg_k,
                                               double s, double u, double rho1) {
  double out_weight = neg_mu.dot(neg_k);
  double counts_multiplier = this->s/this->x_sum;

  // // rho2 is due to the Lagrangian from the boundary constraint
  Row_Vector P_diag = (pos_mu * (s * out_weight)).array().pow(2) / this->x_sum;

  // there are two columns, corresponding to the primal and the Lagrangian arising from the equality constraint
  double primal = s * sqrt(neg_k.array().pow(2).matrix().dot(neg_mu.array().pow(2).matrix()) / this->x_sum);
  double lagrangian = sqrt(rho1);

  Col_Vector p_l(2,1);
  p_l << primal, lagrangian;

  Dense_Matrix P_low_rank = (p_l * pos_mu).transpose();

  // # P = np.diag(P_diag) + P_low_rank.dot(P_low_rank.T)
  Col_Vector resid_in = pos_co_counts - s * neg_mu.sum() * pos_mu.transpose();
  Col_Vector resid_out = neg_co_counts - s * pos_mu.sum() * neg_mu.transpose();

  double diff = u - out_weight;

  // How the fuck did this actually work?
  Col_Vector q =
    counts_multiplier * out_weight * (resid_in.array() * pos_mu.transpose().array()).matrix()
  + counts_multiplier * (resid_out.array() * neg_mu.transpose().array() * neg_k.transpose().array()).sum() * pos_mu.transpose()
  + rho1 * diff * pos_mu.transpose();

  double r = 0.5 * ((resid_in.array().pow(2)).sum() + (resid_out.array().pow(2)).sum()) / this->x_sum
       + 0.5 * rho1 * pow(diff, 2);

  return make_tuple(P_diag, P_low_rank, q, r);
}
