#ifndef BAYESLEX
#define BAYESLEX 1

// STD headers
#include <string>
#include <vector>
#include <map>
#include <tuple>
#include <memory>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
using namespace std;

// C headers
#include <glob.h>

// Boost
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/trim.hpp>

// Eigen Headers and typedefs
#include <Eigen/Sparse>
#include <Eigen/Dense>
#include <Eigen/StdVector>
// Matrix typedefs
using CSR_Matrix   = Eigen::SparseMatrix<double, Eigen::RowMajor>;
using CSC_Matrix   = Eigen::SparseMatrix<double, Eigen::ColMajor>;
using Dense_Matrix = Eigen::MatrixXd;
// Vector typedefs
using Row_Vector   = Eigen::Matrix <double,   1, -1>;   // Row Vector
using Col_Vector   = Eigen::Matrix <double,  -1,  1>;   // Column Vector
using MapVec       = Eigen::Map    <Row_Vector>;        // Maps std::vec to Row_Vector
// Array typedefs
using Row_Array    = Eigen::Array  <double,   1, -1>;   // Row Array
using Col_Array    = Eigen::Array  <double,  -1,  1>;   // Column Array
// Template bindings
template <typename T>
using Row_Array_T  = Eigen::Array  <T,   1, -1>;   // Row Array
template <typename T>
using Col_Array_T  = Eigen::Array  <T,  -1,  1>;   // Column Array
template <typename T>
using Triplet      = Eigen::Triplet<T>;

namespace BayesLex {
  namespace Data {
    unique_ptr<vector<string>>
      glob(const string& path_prefix);
    unique_ptr<vector<pair<string, int>>>
      get_word_counts(const string& line);
    tuple<Row_Vector, CSR_Matrix, map<string,double>>
      load_data(const string& path_prefix, const int& max_vocab);
    Row_Vector
      get_lexicon(const string& lexfile, const map<string, double>& vocab);
  }

  namespace Baseline {
    Row_Vector
      get_lex_classifier(const Row_Vector& poslex,
                         const Row_Vector& neglex,
                         const map<string, double>& vocab);
  }

  namespace Eval {
    double
      three_class_accuracy(const Row_Vector& labels,
                           const Row_Array_T<bool>& predictions);
    string
      get_results(const Row_Vector& labels,
                  const Row_Vector& predictions,
                  const string& approach);
  }

  namespace Stats {
    Row_Vector
      scale(Row_Vector& pred, CSR_Matrix& x);
    tuple<CSR_Matrix, CSR_Matrix>
      getCoCountsTwoLex(const CSR_Matrix& x,
                        const Row_Vector& poslex,
                        const Row_Vector& neglex,
                        const double s);
    double
      computeS(const CSR_Matrix& x);
    double
      estimateK(const tuple<CSR_Matrix, CSR_Matrix>& co_counts);
    Row_Vector
      eMu(const CSR_Matrix& x);
    Row_Vector
      makePredictionsKPerWord(const CSR_Matrix& x,
                              const Row_Vector& poslex,
                              const Row_Vector& neglex,
                              const Row_Vector& pos_k,
                              const Row_Vector& neg_k);
    Row_Vector
      computeR(const CSR_Matrix& x,
               const Row_Vector& lex,
               const Row_Vector& k);
  }

  namespace ADMM {
    Col_Vector
      lowRankPlusDiagonalSolve(const Col_Vector& d,
                               const Dense_Matrix& u,
                               const Col_Vector& y);
    Dense_Matrix
      lowRankPlusDiagonalQuadProd(const Dense_Matrix& d,
                                   const Dense_Matrix& u,
                                   const Dense_Matrix& x);
    Row_Vector
      admmQuadBounded(const Row_Vector& P_diag,
                      const Dense_Matrix& P_low_rank,
                      Col_Vector q,
                      int max_iter=100,
                      double rho=1.0,
                      double k_max=0.9);
    tuple<double, double>
      updateRho(double u,
                double rho,
                double primal_residual,
                double dual_residual,
                double mu=5.0, double tau_incr=2.0, double tau_decr=2.0);
  }

  namespace Optimize {
    class BayesLexOptimizer{
    public:
        // optimizer state
        const Row_Vector& poslex;  // see main
        const Row_Vector& neglex;  // see main
        int n_pos;      // len(poslex)
        int n_neg;      // len(neglex)
        double s;       //constant term in the expected co-occurence counts for the bayesian model
        double max_k;   //maximum value for predictiveness (default=.9)
        double rval;    //estimated K
        double ratio;   //ratio for pos to neg
        CSR_Matrix co_count;    //cross lexicon co occurence
        CSR_Matrix e_co_count;  //normalized co_counts
        Row_Vector eMu; //expected probabilities for each word
        Row_Vector pos_mu;  //the expected counts for positive words
        Row_Vector neg_mu;  //the expected counts for negative words
        Col_Vector pos_co_counts; //co counts for each pos lex word
        Col_Vector neg_co_counts; //co counts for each neg lex word
        Row_Vector pos_k;  //predictiveness value gamma for each positive word
        Row_Vector neg_k;  //predictiveness value gamma for each negative word
        double x_sum;

        // optimizer functions
        BayesLexOptimizer(const CSR_Matrix& x,
                          const Row_Vector& poslex,
                          const Row_Vector& neglex,
                          double max_k);
        void
          estimateADDM(const int num_epochs,
                     const int max_iter,
                     double rho,
                     const double max_k);
        tuple<Row_Vector, Dense_Matrix, Col_Vector, double>
          getLowRankQuadraticParams(const Col_Vector& pos_co_counts,
                                    const Col_Vector& neg_co_counts,
                                    const Row_Vector& pos_mu,
                                    const Row_Vector& neg_mu,
                                    const Row_Vector& neg_k,
                                    double s, double u, double rho1=1.0);
    };
  }
}

#endif
