#include "BayesLex.hpp"

// solve Ax = y for A = Diad(d) + uu'
//
// dimensions
// d: Kx1
// u: KxN
// y: Kx1
// assume K >> N
Col_Vector
BayesLex::ADMM::lowRankPlusDiagonalSolve(const Col_Vector& d,
                                         const Dense_Matrix& u,
                                         const Col_Vector& y) {
  int K1 = d.rows();
  int K = u.rows();
  int N = u.cols();

  if (K != K1)
    cerr << "Dimension mismatch" << endl;

  // Part 1
  Col_Array d_inv = d.array().inverse();
  Col_Array part1 = d_inv * y.array(); // elementwise multiplication. K x 1

  // Part 2 - This was such a dumb algorithm
  Dense_Matrix d_inv_u(K,N);
  d_inv_u.col(0) = d_inv * u.col(0).array();
  d_inv_u.col(1) = d_inv * u.col(1).array();

  Dense_Matrix nxn = Dense_Matrix::Identity(N,N);
  Dense_Matrix d_inv_tp_u = d_inv_u.transpose() * u;
  Dense_Matrix denom = (nxn.array() + d_inv_tp_u.array()).matrix();

  Dense_Matrix inv_denom = denom.inverse();
  Dense_Matrix left_part = d_inv_u * inv_denom;     // K x N
  Col_Vector right_part = d_inv_u.transpose() * y;  // N x 1
  Col_Array part2 = left_part * right_part;         // K x 1

  return (part1 - part2).matrix();
}

Dense_Matrix
BayesLex::ADMM::lowRankPlusDiagonalQuadProd (const Dense_Matrix& d,
                                             const Dense_Matrix& u,
                                             const Dense_Matrix& x) {
  Dense_Matrix utx = u.transpose() * x;
  return ((x*(d*x)).array() + (utx.transpose()*utx).array()).matrix();
}

Row_Vector
BayesLex::ADMM::admmQuadBounded(const Row_Vector& P_diag,
                                const Dense_Matrix& P_low_rank,
                                Col_Vector q,
                                int max_iter, double rho, double k_max) {
  Col_Vector a = Col_Vector::Zero(q.rows());
  Col_Vector v = Col_Vector::Zero(q.rows());

  Col_Vector P_diag_plus_rho = (P_diag.transpose().array() + rho).matrix();

  Col_Vector x;

  for (int i = 0; i < max_iter; i++) {
    x = lowRankPlusDiagonalSolve(P_diag_plus_rho, P_low_rank, q * -1);
    q =  (q.array() - (v.array()-a.array()) * rho).matrix();
    a =  (x.array() + v.array()).unaryExpr([& k_max](double d){
      return std::max(0.0, std::min(d, k_max));
    }).matrix();
    v =  (v.array() + (x.array()-a.array())).matrix();
    q =  (q.array() + (v.array()-a.array()) * rho).matrix();
  }

  return x.array().unaryExpr([& k_max](double d){
    return std::max(0.0, std::min(d, k_max));
  }).matrix().transpose();
}

// Adaptive computation of ADMM Penalty
tuple<double, double>
BayesLex::ADMM::updateRho(double u,
                          double rho,
                          double primal_residual,
                          double dual_residual,
                          double mu, double tau_incr, double tau_decr) {
  if (primal_residual > mu * dual_residual) {
    rho *= tau_incr;
    u /= tau_incr;
  } else if (dual_residual < mu * primal_residual) {
    rho /= tau_decr;
    u *= tau_decr;
  }
  return tuple<double, double>(rho, u);
}
