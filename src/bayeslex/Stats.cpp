#include "BayesLex.hpp"

Row_Vector
BayesLex::Stats::scale(Row_Vector& pred, CSR_Matrix& x) {
  Row_Vector row_sums = (x * Col_Vector::Ones(x.cols())).transpose();
  row_sums.unaryExpr(
    [] (const double d) { return d + 0.1; }
  );
  return (pred.array() / row_sums.array()).matrix();
}

//produces co-occurence counts for words in the positive and negative lexicon
tuple<CSR_Matrix, CSR_Matrix>
BayesLex::Stats::getCoCountsTwoLex(const CSR_Matrix& x,
                                   const Row_Vector& poslex,
                                   const Row_Vector& neglex,
                                   const double s) {
  // We need Col major order to that the columns are assignable
  CSC_Matrix pos_x(x.rows(), poslex.size()); //columns in x that appear in the pos lex
  CSC_Matrix neg_x(x.rows(), neglex.size()); //columns in x that appear in the neg lex

  //filter x for pos
  for(int i=0; i < poslex.size(); ++i){
    pos_x.col(i) = x.col((int)poslex[i]);
  }
  for(int i=0; i < neglex.size(); ++i){
    neg_x.col(i) = x.col((int)neglex[i]);
  }

  // Raw CO counts
  CSR_Matrix co_counts(pos_x.transpose() * neg_x);
  CSR_Matrix pos_rm(pos_x);
  CSR_Matrix neg_rm(neg_x);

  // Normalized CO counts, 1 x N
  Row_Vector pos_mu = (Row_Vector::Ones(pos_rm.rows()) * pos_rm) / x.sum();
  Row_Vector neg_mu = (Row_Vector::Ones(neg_rm.rows()) * neg_rm ) / x.sum();

  CSR_Matrix e_co_counts = (pos_mu.transpose() * neg_mu).sparseView() * s;
  return make_tuple(co_counts, e_co_counts);
}

//s is a constant term in the expected co-occurence counts for the Bayesian model
double
BayesLex::Stats::computeS(const CSR_Matrix& x){
  Row_Vector N_sum = (x * Col_Vector::Ones(x.cols())).transpose();

  // for coeff d in N_sum, compute coeff-wise product of d(d-1)
  float s = N_sum.unaryExpr([](const double& d) {
    return d*(d-1);
  }).sum();

  return s;
}

//estimates a single predictiveness parameter for all words
double
BayesLex::Stats::estimateK(const tuple<CSR_Matrix, CSR_Matrix>& co_counts) {
  CSR_Matrix co_count = get<0>(co_counts);
  CSR_Matrix e_co_count = get<1>(co_counts);

  return sqrt(e_co_count.sum() / co_count.sum() - 1);
}

//produces the expected means for each word
// [1, 1, ... 1] * X
// Sum over columns / total sum
Row_Vector
BayesLex::Stats::eMu(const CSR_Matrix& x){
  Row_Vector res = (Row_Vector::Ones(x.rows()) * x) / x.sum();
  return res;
}

//make predictions based on the decision rule (13) from the paper
Row_Vector
BayesLex::Stats::makePredictionsKPerWord(const CSR_Matrix& x,
                                         const Row_Vector& poslex,
                                         const Row_Vector& neglex,
                                         const Row_Vector& pos_k,
                                         const Row_Vector& neg_k){

  //represents R for all positive words
  Row_Vector pos_r = BayesLex::Stats::computeR(x, poslex, pos_k);
  //represents R for all negative words
  Row_Vector neg_r = BayesLex::Stats::computeR(x, neglex, neg_k);
  //returns the decision rule for each document (positive sentiment if > 0, negative sentiment if < 0)
  return (pos_r - neg_r);
}

//compute the prediction rule for a given lexicon
Row_Vector
BayesLex::Stats::computeR(const CSR_Matrix& x, const Row_Vector& lex, const Row_Vector& k){
  CSC_Matrix selection(x.rows(), lex.size());

  //filter x for words in lex
  for(int i=0; i < lex.size(); ++i){
    selection.col(i) = x.col(lex[i]);
  }

  Row_Vector weights = k.unaryExpr([](const double& d) {
    return log(1+d)-log(1-d);
  });

  Row_Vector res = selection*weights.transpose();
  return res;
}
