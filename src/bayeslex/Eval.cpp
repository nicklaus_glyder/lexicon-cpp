#include "BayesLex.hpp"

// Recall of classifier
double
BayesLex::Eval::three_class_accuracy(const Row_Vector& labels,
                                     const Row_Array_T<bool>& predictions) {
  auto tp = ((labels.array() > 0) && predictions).count();
  auto tn = (labels.array() > 0).cwiseEqual(predictions).count() - tp;
  return (double)(tp + tn)/labels.size();
}

string
BayesLex::Eval::get_results(const Row_Vector& labels,
                            const Row_Vector& predictions,
                            const string& approach) {
  stringstream ss;
  double acc = three_class_accuracy(labels, predictions.array() > 0);
  ss << "--- " << approach << " recall: " << acc << endl;
  return ss.str();
}
