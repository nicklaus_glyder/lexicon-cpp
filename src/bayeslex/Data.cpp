#include "BayesLex.hpp"

// Glob a filepath, adapted from
// https://stackoverflow.com/questions/8401777/simple-glob-in-c-on-unix-system
unique_ptr<vector<string>>
BayesLex::Data::glob(const string& path_prefix) {
  glob_t glob_result;
  glob(path_prefix.c_str(), GLOB_TILDE, NULL, &glob_result);
  auto ret = unique_ptr<vector<string>> ( new vector<string>() );
  for(uint i=0; i < glob_result.gl_pathc; i++){
    ret->emplace_back(string(glob_result.gl_pathv[i]));
  }
  globfree(&glob_result);
  return ret;
}

// Given a BoW line, return pairs of word:count
unique_ptr<vector<pair<string, int>>>
BayesLex::Data::get_word_counts(const string& line) {
  istringstream tokens(line);
  auto ret = unique_ptr<vector<pair<string, int>>>(
    new vector<pair<string, int>>()
  );

  string token;
  string count;
  while(getline(tokens, token, ':') && getline(tokens, count, ' ')) {
    if (boost::all(token, boost::is_alpha()) && token != "")
      ret->emplace_back(pair<string,int>(token, stoi(count)));
  }

  return ret;
}

// Load BoW and Key, generate training data
tuple<Row_Vector, CSR_Matrix, map<string,double>>
BayesLex::Data::load_data(const string& path_prefix, const int& max_vocab) {
  ifstream BoW(path_prefix+".bow");
  ifstream Key(path_prefix+".key");

  map <string,double>      vocab_counter;   // Maps unique words to counts across corpus
  map <string,double>      vocab;           // Maps unique words to rank (most -> least freq.)
  vector<double>           labels;          // Maps doc to POS or NEG lexicon [1,-1]
  vector<Triplet<double>>  doc_word_counts; // Triplets used to make SparseMatrix

  // Did files open?
  if (!BoW || !Key) {
    cerr << "Couldn't find BoW and/or Key: prefix = " << path_prefix << endl;
    exit(1);
  }

  // Read the BoW and generate a vocabulary
  string bow_line;
  while (BoW >> bow_line) {
    auto word_counts = BayesLex::Data::get_word_counts(bow_line);
    for ( pair<string, double> const& token : *word_counts ) {
      auto key = vocab_counter.find(token.first);
      if ( key == vocab_counter.end() ) {
        vocab_counter[token.first] = token.second;  // First entry
      } else {
        key->second += token.second; // have already seen this word
      }
    }
  }

  // This takes the most frequently used words <= max_vocab
  vector<pair<string, double>> vocab_list(min((int)vocab_counter.size(), max_vocab));
  partial_sort_copy(vocab_counter.begin(), vocab_counter.end(),
                    vocab_list.begin(), vocab_list.end(),
                     [](std::pair<const std::string, double> const& l,
                        std::pair<const std::string, double> const& r)
                     {
                         return l.second > r.second;
                     });

  // Swap word count for rank in vocab
  for (uint i = 0; i < vocab_list.size(); i++)
    vocab[vocab_list[i].first] = i;

  // Generate other indices and matrices
  BoW.clear();
  BoW.seekg(0, BoW.beg);

  string key_line;
  int t = 0;
  while (getline(BoW, bow_line) && getline(Key, key_line)) {
    string label = key_line.substr(key_line.size() - 3);
    label == "POS" ? labels.emplace_back(1) : labels.emplace_back(-1);
    auto word_counts = BayesLex::Data::get_word_counts(bow_line);
    for ( pair<string, int> const& token : *word_counts ) {
      auto key = vocab.find(token.first);
      if ( key != vocab.end() ) {
        doc_word_counts.emplace_back(Triplet<double>(t, key->second, token.second));
      }
    }
    t+=1;
  }

  // Using Eigen SparseMatrix
  CSR_Matrix x(labels.size(), vocab.size());
  x.setFromTriplets(doc_word_counts.begin(), doc_word_counts.end());

  return make_tuple(MapVec(labels.data(), labels.size()), x, vocab);
}

Row_Vector
BayesLex::Data::get_lexicon(const string& lexfile, const map<string, double>& vocab) {
    ifstream lex_in(lexfile);
    vector<double> indices;
    string word;

    if (!lex_in) {
      cerr << "Could not open lexicon file: " << lexfile << endl;
      exit(1);
    }

    while (lex_in >> word) {
      boost::trim_right(word);
      auto key = vocab.find(word);
      if ( key != vocab.end() )
          indices.emplace_back(key->second);
    }

    sort(indices.begin(), indices.end());
    return MapVec(indices.data(), indices.size());
  }
